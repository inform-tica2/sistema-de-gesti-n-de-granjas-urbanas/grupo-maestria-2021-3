var mymap = L.map('mapid');
mymap.setView([4.626894, -74.173802], 13);

var provider = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
	maxZoom: 19,
	attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
});

provider.addTo(mymap);


var marker = L.marker([4.626894, -74.173802]).addTo(mymap);
var marker2 = L.marker([4.6, -74.173]).addTo(mymap);
var marker3 = L.marker([4.7, -74.173]).addTo(mymap);

var circle = L.circle([4.626894, -74.173802], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 500
}).addTo(mymap);


